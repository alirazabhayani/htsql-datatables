HTSQL/DataTables Integration
============================

This project is just an example of how to integrate the `jQuery
DataTables plugin <http://datatables.net/>`_ with an `HTSQL
<http://htsql.org/>`_ web service.

Ultimately it would be wonderful if the `HTRAF JavaScript library
<http://htsql.org/htraf/index.html>`_ had a datatable widget with
options for both client-side and server-side paging and
sorting. Unfortunately I don't have the JavaScript skills to figure
out how to properly integrate DataTables with HTRAF, so this project
is more of a hack to show it is at least possible.

Demo
----

To run the demo server, Python >= 2.6 is required, and HTSQL >=
2.3.1. E.g.::

       $ pip install --upgrade htsql # may need sudo
       $ git clone https://bitbucket.org/alimanfoo/htsql-datatables.git
       $ cd htsql-datatables
       $ python serve.py sqlite:htsql_demo.sqlite localhost 8080

...then browse to: http://localhost:8080/
